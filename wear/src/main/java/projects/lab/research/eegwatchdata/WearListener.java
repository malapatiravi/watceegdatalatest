package projects.lab.research.eegwatchdata;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.ByteArrayOutputStream;

/**
 * Created by SuzzettPdl on 6/22/2016.
 */
public class WearListener extends WearableListenerService
{
  private final String TAG = "WearListener";
  @Override
  public void onDataChanged(DataEventBuffer dataEventBuffer)
  {
    Log.i(TAG, "data changed in actual listener");
    DataMap dataMap;
    for (DataEvent event : dataEventBuffer)
    {
      if (event.getType() == DataEvent.TYPE_CHANGED) {
        // Check the data path
        String path = event.getDataItem().getUri().getPath();
        if (path.equals(SharedData.FROM_PHONE_DATA_PATH))
        {
          dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
          Log.i(TAG, "DataMap received for watch: " + dataMap);
          notifyWatch("EEG Watch", "Datamap received for watch"+dataMap);

          String command = dataMap.get(SharedData.COMMAND_TAG);
          if (command.equals(SharedData.START_DC))
          {
            String nodeID = event.getDataItem().getUri().getHost();
            Intent activityBroadcastIntent = new Intent("serviceCommands");
            activityBroadcastIntent.putExtra(SharedData.COMMAND_TAG, SharedData.START_DC);
            activityBroadcastIntent.putExtra(SharedData.DATA_FILE_PREFIX_TAG, dataMap.get(SharedData.DATA_FILE_PREFIX_TAG).toString());
            activityBroadcastIntent.putExtra(SharedData.DATA_FOLDER_TAG, dataMap.get(SharedData.DATA_FOLDER_TAG).toString());
            activityBroadcastIntent.putExtra(SharedData.TIME_TAG, dataMap.get(SharedData.TIME_TAG).toString());
            activityBroadcastIntent.putExtra(SharedData.NODE_ID_TAG, nodeID);
            sendBroadcast(activityBroadcastIntent);
          }
          else if (command.equals(SharedData.STOP_DC))
          {
            Intent activityBroadcastIntent = new Intent("serviceCommands");
            activityBroadcastIntent.putExtra(SharedData.COMMAND_TAG, SharedData.STOP_DC);

            sendBroadcast(activityBroadcastIntent);
          }
          else if (command.equals(SharedData.DISCONNECT_WATCH_CMD))
          {
            Intent activityBroadcastIntent = new Intent("serviceCommands");
            activityBroadcastIntent.putExtra(SharedData.COMMAND_TAG, SharedData.FINISH_WEAR_ACTIVITY);
            sendBroadcast(activityBroadcastIntent);
          }
          else if (command.equals(SharedData.CONNECT_WATCH_CMD))
          {
            Intent launcherIntent = new Intent(this, WearMainActivity.class);
            launcherIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(launcherIntent);
          }
          else
          {
            Log.i(TAG, "Unrecognized command");
          }
        }
        else
        {
          //Log.i(TAG, "Data received but it is not from phone");
        }
      }
    }
  }
  public void notifyWatch(String title, String message)
  {
    int notificationId = 001;
    String EXTRA_EVENT_ID = "extra_event";
    int eventId = 001;
    // Build intent for notification content
    Intent viewIntent = new Intent(this.getApplicationContext(), WearMainActivity.class);
    viewIntent.putExtra(EXTRA_EVENT_ID,eventId);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.getApplicationContext())
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle(title)
            .setContentText(message).extend(new NotificationCompat.WearableExtender()
                                                    .setCustomSizePreset(NotificationCompat.WearableExtender.SIZE_FULL_SCREEN));

    // Get an instance of the NotificationManager service
    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getApplicationContext());
    // Build the notification and issues it with notification manager.
    notificationManager.notify(notificationId,notificationBuilder.build());
  }
}
