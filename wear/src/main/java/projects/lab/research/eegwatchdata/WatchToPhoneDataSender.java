package projects.lab.research.eegwatchdata;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Wearable;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by suzzett on 7/19/16.
 */
public class WatchToPhoneDataSender implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
  private String TAG = "WatchToPhoneDataSender";
  private GoogleApiClient mGoogleClient;
  Context context;
  private String bestNodeID = "";
  private final int MAX_TRANSFER_BYTES = 512000;

  public WatchToPhoneDataSender(Context context, String bestNodeID)
  {
    this.context = context;
    setBestNode(bestNodeID);
    setupGoogleAPI();
  }

  public void setBestNode(String bestNodeID)
  {
    this.bestNodeID = bestNodeID;
  }

  @Override
  public void onConnected(Bundle bundle)
  {
    Log.i(TAG, "Google API connected");
    //Wearable.DataApi.addListener(mGoogleClient, WearListener.class);
  }

  @Override
  public void onConnectionSuspended(int i)
  {
    Log.i(TAG, "Google API connection suspended");
  }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult)
  {
    Log.i(TAG, "Google API connection failed: "+connectionResult.getErrorCode());
    Log.i(TAG, "Google API connection failed: "+connectionResult.getErrorMessage());
  }
  private void setupGoogleAPI()
  {
    mGoogleClient = new GoogleApiClient.Builder(context).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Wearable.API).build();
    mGoogleClient.connect();
  }

  public void sendBytes(ArrayList<String> fullData, String path)
  {
    byte[] byteData = arrayListToByteArray(fullData);
    sendDataAsBytes(byteData, path);
  }

  public void packetAndSendBytes(ArrayList<String> fullData, String path)
  {
    ArrayList<String> tmpList = new ArrayList<>();
    int bytes = 0;
    int packetNum = 0;
    tmpList.add("Pack-"+packetNum);
    for (int i = 0; i < fullData.size(); i++)
    {
      bytes += fullData.get(i).getBytes().length;
      tmpList.add(fullData.get(i));
      if ((bytes>=MAX_TRANSFER_BYTES) || (i>=(fullData.size()-1)))
      {
        byte[] byteData = arrayListToByteArray(tmpList);
        sendDataAsBytes(byteData, path);
        bytes = 0;
        packetNum++;
        tmpList.clear();
        tmpList.add("Pack-"+packetNum);
      }
    }
    tmpList.clear();
    tmpList.add("Last-"+packetNum);
    byte[] byteData = arrayListToByteArray(tmpList);
    sendDataAsBytes(byteData, path);
  }
  private byte[] arrayListToByteArray(ArrayList<String> data)
  {
    // write to byte array
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream out = new DataOutputStream(baos);
    for (String element : data)
    {
      try                     {out.writeUTF(element);}
      catch (IOException e)   {e.printStackTrace();}
    }
    byte[] bytes = baos.toByteArray();
    return bytes;
  }


  private void sendDataAsBytes(byte[] dataToSend, String path)
  {
    SendData dataSender = new SendData(path, dataToSend);
    dataSender.start();
  }
  //This class is here just because await cannot be run in UI thread...
  private class SendData extends Thread
  {
    String path;
    byte[] dataToSend;
    public SendData(String path, byte[] data)
    {
      this.path = path;
      this.dataToSend = data;
    }
    @Override
    public void run()
    {
      putByteArrayToLayer(this.path, this.dataToSend);
    }
  }

  private void putByteArrayToLayer(String path, final byte[] dataToSend)//DataMap dataMap)
  {
    if (bestNodeID != null)
    {
      Log.i(TAG, "Sending data now to path: "+path);
      //final byte[] dataToSend = dataMap.toByteArray();
      if (mGoogleClient == null) setupGoogleAPI();
      Log.i(TAG, "Size of payload: "+(dataToSend.length)+" bytes");
      Wearable.MessageApi.sendMessage(mGoogleClient, bestNodeID, path, dataToSend).setResultCallback
      (
        new ResultCallback()
        {
          @Override
          public void onResult(Result result)
          {
            if (!result.getStatus().isSuccess()) {
              Log.i(TAG, "Failed to send the message: "+result.getStatus().getStatusMessage());
            }
            else
            {

              //notifyWatch("EEG Watch", "Sending data ("+(dataToSend.length/1024)+" KB) sucessful");
              Log.i(TAG, "Sending data ("+(dataToSend.length)+" bytes) sucessful");
            }
          }
        }
      );
    }
    else
    {
      Log.d(TAG, "No connected node found...");
    }
  }

}
