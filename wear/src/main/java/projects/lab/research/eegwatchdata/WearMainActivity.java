package projects.lab.research.eegwatchdata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Wearable;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class WearMainActivity extends WearableActivity
{

  private String TAG = "WearMainActivity";
  private String dataFolder = "default";
  private String fileNamePrefix = "One";
  private String bestNodeID = "";
  private SensorDataCollector sdc;
  private String watchStartTime = "";
  Context context;

  private TextView statusField;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_wear_main);
    context = this;
    statusField = (TextView) findViewById(R.id.watch_status_tv);
    setAmbientEnabled();
    registerReceiver(serviceListener, new IntentFilter("serviceCommands"));
  }

  @Override
  public void onEnterAmbient(Bundle ambientDetails)
  {
    super.onEnterAmbient(ambientDetails);
    updateDisplay();
  }

  @Override
  public void onUpdateAmbient()
  {
    super.onUpdateAmbient();
    updateDisplay();
  }

  @Override
  public void onExitAmbient()
  {
    updateDisplay();
    super.onExitAmbient();
  }

  private void updateDisplay()
  {
    if (isAmbient()) {}else {}
  }

  @Override
  protected void onDestroy()
  {
    unregisterReceiver(serviceListener);
    super.onDestroy();
  }

  public synchronized void startSensorsDataCollection()
  {
    updateStatusMsg("Started data collection");
    sdc = new SensorDataCollector(context, dataFolder, fileNamePrefix, bestNodeID, watchStartTime);
    sdc.initializeSensors();
  }
  private synchronized void stopSensorsDataCollection()
  {
    updateStatusMsg("Stopping data collection...");
    sdc.stopCollection();
    updateStatusMsg("Sending data...");
    sdc.sendDataToPhone();
  }
  private final BroadcastReceiver serviceListener = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      Bundle intentExtras = intent.getExtras();
      if (intentExtras.containsKey(SharedData.COMMAND_TAG))
      {
        String command = intent.getStringExtra(SharedData.COMMAND_TAG);
        if (command.equals(SharedData.START_DC))
        {
          fileNamePrefix = intent.getStringExtra(SharedData.DATA_FILE_PREFIX_TAG);
          watchStartTime = intent.getStringExtra(SharedData.TIME_TAG);
          dataFolder = intent.getStringExtra(SharedData.DATA_FOLDER_TAG);
          bestNodeID = intent.getStringExtra(SharedData.NODE_ID_TAG);
          startSensorsDataCollection();
        }
        else if (command.equals(SharedData.FINISH_WEAR_ACTIVITY))
        {
          finish();
        }
        else if (command.equals(SharedData.STOP_DC))
        {
          stopSensorsDataCollection();
        }
      }
      else if (intentExtras.containsKey(SharedData.MESSAGE_TAG))
      {
        String msg = intentExtras.getString(SharedData.MESSAGE_TAG);
        updateStatusMsg(msg);
      }
    }
  };

  public void updateStatusMsg(String msg)
  {
    statusField.setText(msg);
    notifyWatch("EEG Watch", msg);
  }

  private int notificationId = 0;
  public void notifyWatch(String title, String message)
  {
    notificationId++;
    String EXTRA_EVENT_ID = "extra_event";
    int eventId = 001;
    // Build intent for notification content
    Intent viewIntent = new Intent(this.getApplicationContext(), WearMainActivity.class);
    viewIntent.putExtra(EXTRA_EVENT_ID,eventId);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.getApplicationContext())
            .setSmallIcon(R.drawable.ic_launcher)
            .setContentTitle(title)
            .setContentText(message).extend(new NotificationCompat.WearableExtender()
                                                    .setCustomSizePreset(NotificationCompat.WearableExtender.SIZE_FULL_SCREEN));

    // Get an instance of the NotificationManager service
    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getApplicationContext());
    // Build the notification and issues it with notification manager.
    notificationManager.notify(notificationId,notificationBuilder.build());
  }
}
