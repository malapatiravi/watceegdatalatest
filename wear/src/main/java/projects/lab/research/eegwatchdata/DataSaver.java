package projects.lab.research.eegwatchdata;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.google.android.gms.wearable.Asset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by SuzzettPdl on 4/28/2016.
 */
public class DataSaver
{
  private final String ROOT_FOLDER = "WatchEEG_Data";
  private String FolderName;
  private String Filename;
  private String Root;
  private File Folder;
  private File ThisFile;
  private FileWriter FW;
  private PrintWriter PW;
  private String TAG = "DataSaver";
  private Context context;
  //private ConcurrentLinkedQueue<String> dataQueue;
  private List<String> dataQueue;
  private boolean startDataSaving = false;
  private WearMainActivity wma;

  public DataSaver(String FolderLoc, String FN, Context context)
  {
    this.context = context;
    wma = (WearMainActivity) this.context;
    dataQueue = new ArrayList<>();//Collections.synchronizedList(new ArrayList<String>());
    Log.i(TAG, "Filename="+FN+", Folder="+FolderLoc);
    if (!isExternalStorageWritable()) Log.e(TAG, "External storage is not writable");
    Root = Environment.getExternalStorageDirectory().toString();//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString();
    File rootDir = Environment.getExternalStorageDirectory();//Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);//Environment.getExternalStorageDirectory();
    if (!rootDir.canWrite())
    {
      Log.d(TAG, "cannot write in: "+rootDir.toString()+", space: "+rootDir.getUsableSpace()/(1024*1024*1024)+"GB, can read: "+rootDir.canRead()+", list: "+rootDir.list()+", Exists: "+rootDir.exists());
    }
    FolderName = FolderLoc;
    Filename = FN;
    Folder = new File(Root+"/"+ROOT_FOLDER+"/"+FolderName);
    if (!Folder.exists())
    {
      Log.i(TAG, "Dir does not exist, making one.");
      if (Folder.mkdirs()) Log.i(TAG, "Mkdir succeeded");
      else Log.i(TAG, "Mkdir failed");
    }
    String CompleteFilename = Folder+"/"+Filename;
    ThisFile = new File(Folder , Filename);
    if (ThisFile.exists ()) ThisFile.delete ();
    try                   {FW = new FileWriter(CompleteFilename);}
    catch (IOException e) {e.printStackTrace();}
    PW = new PrintWriter(FW);
    startDataSaving = true;
  }

  class SaveDataToFileThread extends Thread
  {
    SaveDataToFileThread()
    {

    }
    @Override
    public void run()
    {
      int dataSize;
      do
      {
        dataSize = dataQueue.size();
        if (dataSize > 0) SaveData(dataQueue.remove(0));
      } while((dataSize > 0));
    }

  }
  public void addToDataQueue(String data)
  {
    dataQueue.add(data);
  }
  /* Checks if external storage is available for read and write */
  public boolean isExternalStorageWritable()
  {
    String state = Environment.getExternalStorageState();
    if (Environment.MEDIA_MOUNTED.equals(state))
    {
      return true;
    }
    return false;
  }

  public ArrayList<String> getAllData()
  {
    this.Close();
    ArrayList<String> data = new ArrayList<>();
    try
    {
      BufferedReader r = new BufferedReader(new FileReader(Folder +"/"+ Filename));
      int totalLines = 0;
      String line;
      while ((line = r.readLine()) != null)
      {
        totalLines++;
        data.add(line);
      }
      Log.i(TAG, "Reading file. Total lines: "+totalLines +", size: "+data.size());
      return data;
    } catch (FileNotFoundException e)
    {
      e.printStackTrace();
      return null;
    }
    catch (IOException e)
    {
      e.printStackTrace();
      return null;
    }
  }

  private BufferedReader currentReadBuffer = null;
  private int maxBytes = 0;
  public void initializeGetNextXBytes(int maxBytes)
  {
    try {currentReadBuffer = new BufferedReader(new FileReader(Folder +"/"+ Filename));}
    catch (FileNotFoundException e) {e.printStackTrace();}
    this.maxBytes = maxBytes;
  }
  public ArrayList<String> getNextXBytes()
  {
    this.Close();
    ArrayList<String> data = new ArrayList<>();
    try
    {
      String line;
      int bytesSize = 0;
      while ((line = currentReadBuffer.readLine()) != null)
      {
        bytesSize += line.length();
        data.add(line);
        if (bytesSize >= maxBytes) break;
      }
      return data;
    }
    catch (IOException e)
    {
      e.printStackTrace();
      return null;
    }
  }

  public byte[] getAllDataInBytes(String dataType)
  {
    this.Close();
    String allDataStr = dataType;
    byte[] allData;
    Scanner s = null;
    try
    {
      File fileToRead = new File(Folder , Filename);
      s = new Scanner(fileToRead);
      int totalLines = 0;
      while (s.hasNext())
      {
        allDataStr += "::"+s.next();
        totalLines++;
      }
      allData = allDataStr.getBytes();
      Log.i(TAG, "Reading file: "+fileToRead.getAbsolutePath()+", total lines: "+totalLines +", size: "+allData.length);
      s.close();
      return allData;
    } catch (FileNotFoundException e)
    {
      e.printStackTrace();
      return null;
    }
  }

  public boolean SaveData(String SS)
  {
    PW.write(SS);
    PW.println();
    PW.flush();
    return true;
  }

  public void saveAndClose()
  {
    new SaveDataAndCloseFile().start();
  }

  public static boolean savingFile = false;
  private class SaveDataAndCloseFile extends Thread
  {
    @Override
    public void run()
    {
      savingFile = true;
      saveDataToFile();
      Close();
      savingFile = false;
    }
  }
  private void saveDataToFile()
  {
    int dataSize = dataQueue.size();
    do
    {
      dataSize = dataQueue.size();
      if (dataSize > 0) SaveData(dataQueue.remove(0));
    } while((dataSize > 0));
    sendUpdateOnUIThread(Filename+" saving complete: ");
  }

  private void sendUpdateOnUIThread(final String msg)
  {
    wma.runOnUiThread(new Runnable() {
      @Override
      public void run()
      {
        wma.updateStatusMsg(msg);
      }
    });
  }
  private void Close()
  {
//    startDataSaving = false;
    PW.close();
    try {FW.close();}
    catch (IOException e) {e.printStackTrace();}
  }

  public String GetPath()
  {
    return ThisFile.getAbsolutePath();
  }
}
