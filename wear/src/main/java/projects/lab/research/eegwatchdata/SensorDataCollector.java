package projects.lab.research.eegwatchdata;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by suzzett on 7/19/16.
 */
public class SensorDataCollector
{
  private final String TAG = "SensorDataCollector";
  private final String accFilename = "Acc.txt";
  private final String gyrFilename = "Gyr.txt";
  private final String rovFilename = "RoV.txt";
  private DataSaver accDataSaver;
  private DataSaver gyrDataSaver;
  private DataSaver rovDataSaver;
  private String dataFolder = "default";
  private String fileNamePrefix = "One";
  private String bestNodeID = "";
  private final static int SENS_ACCELEROMETER = Sensor.TYPE_ACCELEROMETER;
  private final static int SENS_GYROSCOPE = Sensor.TYPE_GYROSCOPE;
  private final static int SENS_ROTATION_VECTOR = Sensor.TYPE_ROTATION_VECTOR;
  private final static int SENSOR_DELAY = 10;
  private SensorManager mSensorManager;
  private Context context;
  private WearMainActivity wma;
  private HandlerThread mSensorThread;
  private Handler mSensorHandler;


  public SensorDataCollector(Context context, String dataFolder, String fileNamePrefix, String bestNodeID, String watchStartTime)
  {
    this.context = context;
    wma = (WearMainActivity) context;
    this.dataFolder = dataFolder;
    this.fileNamePrefix = fileNamePrefix;
    this.bestNodeID = bestNodeID;
    setupDataCollectors(watchStartTime);
  }


  private void setupDataCollectors(String watchStartTime)
  {
    accDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + accFilename, context);
    accDataSaver.SaveData("WatchStart:"+watchStartTime);
    gyrDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + gyrFilename, context);
    accDataSaver.SaveData("WatchStart:"+watchStartTime);
    rovDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + rovFilename, context);
    accDataSaver.SaveData("WatchStart:"+watchStartTime);
  }

  public void initializeSensors()
  {
    mSensorThread = new HandlerThread("Sensor thread", Thread.MAX_PRIORITY);
    mSensorThread.start();
    mSensorHandler = new Handler(mSensorThread.getLooper());
    mSensorManager = ((SensorManager) context.getSystemService(context.SENSOR_SERVICE));
    Sensor accelerometerSensor = mSensorManager.getDefaultSensor(SENS_ACCELEROMETER);
    Sensor gyroscopeSensor = mSensorManager.getDefaultSensor(SENS_GYROSCOPE);
    Sensor rotationVectorSensor = mSensorManager.getDefaultSensor(SENS_ROTATION_VECTOR);

    // Register the listener
    if (mSensorManager != null)
    {
      if (accelerometerSensor != null)
      {
        mSensorManager.registerListener(mSensorEventListener, accelerometerSensor, SENSOR_DELAY, mSensorHandler);
      }
      else
      {
        Log.w(TAG, "No Accelerometer found");
      }

      if (gyroscopeSensor != null)
      {
        mSensorManager.registerListener(mSensorEventListener, gyroscopeSensor, SENSOR_DELAY, mSensorHandler);
      }
      else
      {
        Log.w(TAG, "No Gyroscope Sensor found");
      }

      if (rotationVectorSensor != null)
      {
        mSensorManager.registerListener(mSensorEventListener, rotationVectorSensor, SENSOR_DELAY, mSensorHandler);
      }
      else
      {
        Log.d(TAG, "No Rotation Vector Sensor found");
      }
    }
    Log.i(TAG, "Sensors initialized");
  }

  private SensorEventListener mSensorEventListener = new SensorEventListener()
  {
    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
      String dataToSend = (Arrays.toString(sensorEvent.values).replace("[", "").replace("]", "").trim()+","+sensorEvent.timestamp+","+System.currentTimeMillis()).replaceAll("\\s+", "");
      switch (sensorEvent.sensor.getType())
      {
        case (SENS_ACCELEROMETER):
          accDataSaver.addToDataQueue(dataToSend);
          break;
        case (SENS_GYROSCOPE):
          gyrDataSaver.addToDataQueue(dataToSend);
          break;
        case (SENS_ROTATION_VECTOR):
          rovDataSaver.addToDataQueue(dataToSend);
          break;
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }
  };

  public void stopCollection(String watchStopTime)
  {
    mSensorThread.quitSafely();
    accDataSaver.SaveData("WatchStop:"+watchStopTime);
    gyrDataSaver.SaveData("WatchStop:"+watchStopTime);
    rovDataSaver.SaveData("WatchStop:"+watchStopTime);
    Log.i(TAG, "Stopping data collection...");
    if (mSensorManager != null) {mSensorManager.unregisterListener(mSensorEventListener);}
    sendUpdateOnUIThread("Saving and closing acc file");
    accDataSaver.saveAndClose();
    sendUpdateOnUIThread("Saving and closing gyr file");
    gyrDataSaver.saveAndClose();
    sendUpdateOnUIThread("Saving and closing rov file");
    rovDataSaver.saveAndClose();
    sendUpdateOnUIThread("Saving data complete");
  }

  private class SendDataToPhone extends Thread
  {
    @Override
    public void run()
    {
      WatchToPhoneDataSender dataSender = new WatchToPhoneDataSender(context, bestNodeID);

      while (accDataSaver.savingFile) {};
      Log.i(TAG, "Reading acc");
      sendUpdateOnUIThread("Reading acc data to send");
      accDataSaver.initializeGetNextXBytes(SharedData.MAX_TRANSFER_BYTES);
      ArrayList<String> dataArr = accDataSaver.getNextXBytes();
      int packetCounter = 0;
      while (dataArr != null && dataArr.size() > 0)
      {
        dataArr.add(0, "Pack-"+packetCounter);
        packetCounter++;
        dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_ACC);
        dataArr.clear();
        dataArr = accDataSaver.getNextXBytes();
      }
      dataArr.add(0, "Last-"+packetCounter);
      dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_ACC);
      sendUpdateOnUIThread("Reading gyr data to send");

      while (gyrDataSaver.savingFile) {};
      gyrDataSaver.initializeGetNextXBytes(SharedData.MAX_TRANSFER_BYTES);
      dataArr = gyrDataSaver.getNextXBytes();
      packetCounter = 0;
      while (dataArr != null && dataArr.size() > 0)
      {
        dataArr.add(0, "Pack-"+packetCounter);
        packetCounter++;
        dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_GYR);
        dataArr.clear();
        dataArr = gyrDataSaver.getNextXBytes();
      }
      dataArr.add(0, "Last-"+packetCounter);
      dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_GYR);

      while (rovDataSaver.savingFile) {};
      sendUpdateOnUIThread("Reading rov data to send");
      rovDataSaver.initializeGetNextXBytes(SharedData.MAX_TRANSFER_BYTES);
      dataArr = rovDataSaver.getNextXBytes();
      while (dataArr != null && dataArr.size() > 0)
      {
        dataArr.add(0, "Pack-"+packetCounter);
        packetCounter++;
        dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_ROV);
        dataArr.clear();
        dataArr = rovDataSaver.getNextXBytes();
      }
      dataArr.add(0, "Last-"+packetCounter);
      dataSender.sendBytes(dataArr, SharedData.FROM_WEAR_DATA_PATH_ROV);

      ArrayList<String> finishedDataArr = new ArrayList<>();
      finishedDataArr.add("Finished");
      sendUpdateOnUIThread("Done sending data.");
      dataSender.sendBytes(finishedDataArr, SharedData.FROM_WEAR_DATA_PATH_CMD);
    }
  }

  public void sendDataToPhone()
  {
    new SendDataToPhone().start();
  }

//  @Override
//  public void onSensorChanged(SensorEvent sensorEvent)
//  {
//    String dataToSend = (Arrays.toString(sensorEvent.values).replace("[", "").replace("]", "").trim()+","+sensorEvent.timestamp+","+System.currentTimeMillis()).replaceAll("\\s+", "");
//    //Log.i(TAG, "sensor data changed: "+dataToSend);
//    switch (sensorEvent.sensor.getType())
//    {
//      case (SENS_ACCELEROMETER):
//        accDataSaver.addToDataQueue(dataToSend);
//        break;
//      case (SENS_GYROSCOPE):
//        gyrDataSaver.addToDataQueue(dataToSend);
//        break;
//      case (SENS_ROTATION_VECTOR):
//        rovDataSaver.addToDataQueue(dataToSend);
//        break;
//    }
//  }
//
//  @Override
//  public void onAccuracyChanged(Sensor sensor, int i)
//  {
//
//  }

  private void sendUpdateOnUIThread(final String msg)
  {
    wma.runOnUiThread(new Runnable() {
      @Override
      public void run()
      {
        wma.updateStatusMsg(msg);
      }
    });
  }

}
