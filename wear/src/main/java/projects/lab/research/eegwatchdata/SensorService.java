//package projects.lab.research.eegwatchdata;
//
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.os.Bundle;
//import android.os.IBinder;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.app.NotificationManagerCompat;
//import android.util.Log;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.Result;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.wearable.Asset;
//import com.google.android.gms.wearable.CapabilityApi;
//import com.google.android.gms.wearable.CapabilityInfo;
//import com.google.android.gms.wearable.DataApi;
//import com.google.android.gms.wearable.DataMap;
//import com.google.android.gms.wearable.Node;
//import com.google.android.gms.wearable.NodeApi;
//import com.google.android.gms.wearable.PutDataMapRequest;
//import com.google.android.gms.wearable.PutDataRequest;
//import com.google.android.gms.wearable.Wearable;
//
//import java.io.ByteArrayOutputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Set;
//
//public class SensorService extends Service implements SensorEventListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DataSaver.MessageLogger
//{
//  private static final String TAG = "SensorService";
//  private GoogleApiClient mGoogleClient;
//  private final String accFilename = "Acc.txt";
//  private final String gyrFilename = "Gyr.txt";
//  private final String rovFilename = "RoV.txt";
//  private DataSaver accDataSaver;
//  private DataSaver gyrDataSaver;
//  private DataSaver rovDataSaver;
//  private String dataFolder = "default";
//  private String fileNamePrefix = "One";
//  private String bestNodeID = "";
//  private String bestNodeIDInitial = "";
//  private Context context;
//
//
//  private final static int SENS_ACCELEROMETER = Sensor.TYPE_ACCELEROMETER;
//  private final static int SENS_GYROSCOPE = Sensor.TYPE_GYROSCOPE;
//  private final static int SENS_ROTATION_VECTOR = Sensor.TYPE_ROTATION_VECTOR;
//  private final static int SENSOR_DELAY = 10;
//  SensorManager mSensorManager;
//
//  @Override
//  public void onCreate()
//  {
//    super.onCreate();
//    context = this;
//    Log.i(TAG, "Sensor logging will now begin...");
//  }
//
//  @Override
//  public int onStartCommand(Intent intent, int flags, int startId)
//  {
//    fileNamePrefix = intent.getStringExtra(SharedData.DATA_FILE_PREFIX_TAG);
//    dataFolder = intent.getStringExtra(SharedData.DATA_FOLDER_TAG);
//    bestNodeIDInitial = intent.getStringExtra(SharedData.NODE_ID_TAG);
//    bestNodeID = bestNodeIDInitial;
//    setupGoogleAPI();
//    startMeasurement();
//    updateStatusMsg("Started measurements");
//    return START_STICKY;
//  }
//
//  public void updateStatusMsg(String msg)
//  {
//    Intent intent = new Intent("serviceCommands");
//    intent.putExtra(SharedData.MESSAGE_TAG, msg);
//    sendBroadcast(intent);
//  }
//  private void setupGoogleAPI()
//  {
//    mGoogleClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Wearable.API).build();
//    mGoogleClient.connect();
//  }
//
//  @Override
//  public void onConnected(Bundle bundle)
//  {
//    Log.i(TAG, "Google API connected");
//    //Wearable.DataApi.addListener(mGoogleClient, WearListener.class);
//  }
//
//  @Override
//  public void onConnectionSuspended(int i)
//  {
//    Log.i(TAG, "Google API connection suspended");
//  }
//
//  @Override
//  public void onConnectionFailed(ConnectionResult connectionResult)
//  {
//    Log.i(TAG, "Google API connection failed: "+connectionResult.getErrorCode());
//    Log.i(TAG, "Google API connection failed: "+connectionResult.getErrorMessage());
//  }
//  @Override
//  public void onDestroy()
//  {
//    super.onDestroy();
//    Log.i(TAG, "Sensor logging will now end...");
//    stopMeasurement();
//  }
//
//  @Override
//  public IBinder onBind(Intent intent)
//  {
//    return null;
//  }
//
//  public void startMeasurement()
//  {
//    accDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + accFilename, context);
//    gyrDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + gyrFilename, context);
//    rovDataSaver = new DataSaver(dataFolder, fileNamePrefix + "_" + rovFilename, context);
//    //SendData dataSender = new SendData();
//    notifyWatch("EEG Watch", "Started data collection");
//    //dataSender.start();
//    mSensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));
//    Sensor accelerometerSensor = mSensorManager.getDefaultSensor(SENS_ACCELEROMETER);
//    Sensor gyroscopeSensor = mSensorManager.getDefaultSensor(SENS_GYROSCOPE);
//    Sensor rotationVectorSensor = mSensorManager.getDefaultSensor(SENS_ROTATION_VECTOR);
//    // Register the listener
//    if (mSensorManager != null)
//    {
//      if (accelerometerSensor != null)
//      {
//        mSensorManager.registerListener(this, accelerometerSensor, SENSOR_DELAY);
//      }
//      else
//      {
//        Log.w(TAG, "No Accelerometer found");
//      }
//
//      if (gyroscopeSensor != null)
//      {
//        mSensorManager.registerListener(this, gyroscopeSensor, SENSOR_DELAY);
//      }
//      else
//      {
//        Log.w(TAG, "No Gyroscope Sensor found");
//      }
//
//      if (rotationVectorSensor != null)
//      {
//        mSensorManager.registerListener(this, rotationVectorSensor, SENSOR_DELAY);
//      }
//      else
//      {
//        Log.d(TAG, "No Rotation Vector Sensor found");
//      }
//    }
//  }
//
//  public void stopMeasurement()
//  {
//    updateStatusMsg("Stopped measurements");
//    if (mSensorManager != null) {mSensorManager.unregisterListener(this);}
//    notifyWatch("EEG Watch", "Stopped data collection");
//    sendDataToPhone();
//  }
//
//  boolean sendingData = false;
//  private int allDataSentSize = 0;
//  private void sendDataToPhone()
//  {
//    sendingData = true;
//    notifyWatch("EEG Watch", "Reading acc data...");
//    updateStatusMsg("Reading acc");
//    Log.i(TAG, "Reading acc");
//    ArrayList<String> allAccDataArr = accDataSaver.getAllData();
//    Log.i(TAG, "Acc data size: "+allAccDataArr.size());
//    packetAndSendBytes(allAccDataArr, SharedData.SENSOR_ACC_TAG);
//    //byte[] allAccData = arrayListToByteArray(allAccDataArr, SharedData.SENSOR_ACC_TAG);
//    //sendDataAsBytes(allAccData);
//    updateStatusMsg("Reading gyr");
//    Log.i(TAG, "Reading gyr");
//    ArrayList<String> allGyrDataArr = gyrDataSaver.getAllData();
//    Log.i(TAG, "Gyr data size: "+allGyrDataArr.size());
//    packetAndSendBytes(allGyrDataArr, SharedData.SENSOR_GYR_TAG);
//    //byte[] allGyrData = arrayListToByteArray(allGyrDataArr, SharedData.SENSOR_GYR_TAG);
//    //sendDataAsBytes(allGyrData);
//    updateStatusMsg("Reading rov");
//    Log.i(TAG, "Reading rov");
//    ArrayList<String> allRovDataArr = rovDataSaver.getAllData();
//    Log.i(TAG, "Rov data size: "+allRovDataArr.size());
//    packetAndSendBytes(allRovDataArr, SharedData.SENSOR_ROV_TAG);
//    //byte[] allRovData = arrayListToByteArray(allRovDataArr, SharedData.SENSOR_ROV_TAG);
//    //sendDataAsBytes(allRovData);
//
////    notifyWatch("EEG Watch", "Reading acc data...");
////    updateStatusMsg("Reading acc");
////    Asset allAccData = accDataSaver.getFileAsAsset();
////    notifyWatch("EEG Watch", "Reading gyr data...");
////    updateStatusMsg("Reading gyr");
////    Asset allGyrData = gyrDataSaver.getFileAsAsset();
////    notifyWatch("EEG Watch", "Reading rov data...");
////    updateStatusMsg("Reading rov");
////    Asset allRovData = rovDataSaver.getFileAsAsset();
////    sendDataAsAsset(allAccData, SharedData.SENSOR_ACC_TAG);
////    sendDataAsAsset(allGyrData, SharedData.SENSOR_GYR_TAG);
////    sendDataAsAsset(allRovData, SharedData.SENSOR_ROV_TAG);
////    notifyWatch("EEG Watch", "sent 100% of data");
////    ArrayList<String> allAccData = accDataSaver.getAllData();
////    packageAndSendData(allAccData, SharedData.SENSOR_ACC_TAG);
////
////    notifyWatch("EEG Watch", "Reading gyr data...");
////    ArrayList<String> allGyrData = gyrDataSaver.getAllData();
////    packageAndSendData(allGyrData, SharedData.SENSOR_GYR_TAG);
////
////    notifyWatch("EEG Watch", "Reading rov data...");
////    ArrayList<String> allRovData = rovDataSaver.getAllData();
////    packageAndSendData(allRovData, SharedData.SENSOR_ROV_TAG);
////    notifyWatch("EEG Watch", "sent 100% of data ("+allDataSentSize/(1024)+" KB)");
//
//    ArrayList<String> finishedDataArr = new ArrayList<>();
//    finishedDataArr.add("Finished");
//    byte[] msgToSend = arrayListToByteArray(finishedDataArr, SharedData.COMMAND_TAG);
////    DataMap dMap = new DataMap();
////    dMap.putString(SharedData.COMMAND_TAG, "Finished");
//    SendData dataSender = new SendData(SharedData.FROM_WEAR_DATA_PATH, msgToSend);
//    dataSender.start();
//    notifyWatch("EEG Watch", "Sent everything");
//    updateStatusMsg("Sent everything");
//  }
//
//  private void packetAndSendBytes(ArrayList<String> fullData, String dataType)
//  {
//    ArrayList<String> tmpList = new ArrayList<>();
//    int bytes = 0;
//    int packetNum = 0;
//    tmpList.add("Pack-"+packetNum);
//    for (int i = 0; i < fullData.size(); i++)
//    {
//      bytes += fullData.get(i).getBytes().length;
//      tmpList.add(fullData.get(i));
//      if ((bytes>=SharedData.MAX_TRANSFER_BYTES) || (i>=(fullData.size()-1)))
//      {
//        byte[] byteData = arrayListToByteArray(tmpList, dataType);
//        sendDataAsBytes(byteData);
//        bytes = 0;
//        packetNum++;
//        tmpList.clear();
//        tmpList.add("Pack-"+packetNum);
//      }
//    }
//    tmpList.clear();
//    tmpList.add("Last-"+packetNum);
//    byte[] byteData = arrayListToByteArray(tmpList, dataType);
//    sendDataAsBytes(byteData);
//  }
//  private byte[] arrayListToByteArray(ArrayList<String> data, String dataType)
//  {
//    // write to byte array
//    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//    DataOutputStream out = new DataOutputStream(baos);
//    try                     {out.writeUTF(dataType);}
//    catch (IOException e)   {e.printStackTrace();}
//    for (String element : data)
//    {
//      try                     {out.writeUTF(element);}
//      catch (IOException e)   {e.printStackTrace();}
//    }
//    byte[] bytes = baos.toByteArray();
//    return bytes;
//  }
//  private void sendDataAsBytes(byte[] dataToSend)
//  {
//    SendData dataSender = new SendData(SharedData.FROM_WEAR_DATA_PATH, dataToSend);
//    dataSender.start();
//  }
//  private void sendDataAsAsset(Asset dataToSend, String dataTypeTag)
//  {
//    PutDataMapRequest dataMap = PutDataMapRequest.create(SharedData.FROM_WEAR_DATA_PATH);
//    dataMap.getDataMap().putAsset(dataTypeTag, dataToSend);
//    PutDataRequest request = dataMap.asPutDataRequest();
//    PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi.putDataItem(mGoogleClient, request);
//    pendingResult.setResultCallback
//    (
//      new ResultCallback()
//      {
//        @Override
//        public void onResult(Result result)
//        {
//          if (!result.getStatus().isSuccess()) {
//            Log.i(TAG, "Failed to send the message: "+result.getStatus().getStatusMessage());
//            notifyWatch("EEG Watch", "Sending data failed"+result.getStatus().getStatusMessage());
//            sendingData = false;
//          }
//          else
//          {
//
//            //notifyWatch("EEG Watch", "Sending data ("+(dataToSend.length/1024)+" KB) sucessful");
//            Log.i(TAG, "Sending data sucessful");
//            sendingData = false;
//          }
//        }
//      }
//    );
//  }
//  private void packageAndSendData(ArrayList<String> allData, String dataTypeTag)
//  {
//    int initialSize = allData.size();
//    while (allData.size() > 0)
//    {
//      notifyWatch("EEG Watch", "Sending "+dataTypeTag+" data: "+((initialSize-allData.size())*100/initialSize)+"%");
//      ArrayList<String> packetList = new ArrayList<>();
//      DataMap dMap = new DataMap();
//      int totalBytes  = 0;
//      for (int i = 0; i < allData.size(); i++)
//      {
//        String currLine = allData.remove(i);
//        packetList.add(currLine);
//        allDataSentSize += currLine.length();
//        totalBytes += currLine.length();
//        if (totalBytes >= SharedData.MAX_TRANSFER_BYTES) break;
//      }
//      sendingData = true;
//      dMap.putStringArray(dataTypeTag, packetList.toArray(new String[0]));
//      SendData dataSender = new SendData(SharedData.FROM_WEAR_DATA_PATH, dMap);
//      dataSender.start();
//      //while (sendingData){}
//    }
//
//  }
//  @Override
//  public void onSensorChanged(SensorEvent event)
//  {
//    String dataToSend;
//    //DataMap dMap = new DataMap();
//    //Log.i(TAG, "Got sensor data for "+event.sensor.getType());
//    switch (event.sensor.getType())
//    {
//      case (SENS_ACCELEROMETER):
//        dataToSend = Arrays.toString(event.values).replace("[", "").replace("]", "").trim()+","+event.timestamp+","+System.currentTimeMillis();
//        accDataSaver.SaveData(dataToSend.replaceAll("\\s+",""));
//        //dMap.putString(SharedData.SENSOR_ACC_TAG, dataToSend);
//        break;
//      case (SENS_GYROSCOPE):
//        dataToSend = Arrays.toString(event.values).replace("[", "").replace("]", "").trim()+","+event.timestamp+","+System.currentTimeMillis();
//        gyrDataSaver.SaveData(dataToSend.replaceAll("\\s+",""));
//        //dMap.putString(SharedData.SENSOR_GYR_TAG, dataToSend);
//        break;
//      case (SENS_ROTATION_VECTOR):
//        dataToSend = Arrays.toString(event.values).replace("[", "").replace("]", "").trim()+","+event.timestamp+","+System.currentTimeMillis();
//        rovDataSaver.SaveData(dataToSend.replaceAll("\\s+",""));
//        //dMap.putString(SharedData.SENSOR_ROV_TAG, dataToSend);
//        break;
//    }
//    //SharedData.wearSensorDataBuffer.add(dMap);
//  }
//
//
//  @Override
//  public void onAccuracyChanged(Sensor sensor, int accuracy)
//  {
//
//  }
//
//  @Override
//  public void setMessageLog_DS(String msg)
//  {
//    updateStatusMsg(msg);
//  }
//
//
//  //This class is here just because await cannot be run in UI thread...
//  private class SendData extends Thread
//  {
//    String path;
//    DataMap dataMap;
//    byte[] dataToSend;
//    public SendData(String path, DataMap dataMap)
//    {
//      this.path = path;
//      this.dataMap = dataMap;
//      dataToSend = dataMap.toByteArray();
//    }
//    public SendData(String path, byte[] data)
//    {
//      this.path = path;
//      this.dataToSend = data;
//    }
//    @Override
//    public void run()
//    {
//      sendDataToLayer(this.path, this.dataToSend);
//    }
//  }
//  public void sendDataToLayer(String path, final byte[] dataToSend)//DataMap dataMap)
//  {
//    if (bestNodeID != null)
//    {
//      sendingData = true;
//      Log.i(TAG, "Sending data now to path: "+path +", Node: "+bestNodeID+", original: "+bestNodeIDInitial);
//      //final byte[] dataToSend = dataMap.toByteArray();
//      if (mGoogleClient == null) setupGoogleAPI();
//      Log.i(TAG, "Size of payload: "+(dataToSend.length/1024)+" KB");
//      Wearable.MessageApi.sendMessage(mGoogleClient, bestNodeID, path, dataToSend).setResultCallback
//      (
//        new ResultCallback()
//        {
//          @Override
//          public void onResult(Result result)
//          {
//            if (!result.getStatus().isSuccess()) {
//              Log.i(TAG, "Failed to send the message: "+result.getStatus().getStatusMessage());
//              notifyWatch("EEG Watch", "Sending data ("+(dataToSend.length)+" bytes) failed"+result.getStatus().getStatusMessage());
//              sendingData = false;
//            }
//            else
//            {
//
//              //notifyWatch("EEG Watch", "Sending data ("+(dataToSend.length/1024)+" KB) sucessful");
//              Log.i(TAG, "Sending data ("+(dataToSend.length)+" bytes) sucessful");
//              sendingData = false;
//            }
//          }
//        }
//      );
//
//    }
//    else
//    {
//      sendingData = false;
//      Log.d(TAG, "No connected node found...");
//    }
//
//  }
//
//  private int notificationId = 0;
//  public void notifyWatch(String title, String message)
//  {
//    notificationId++;
//    String EXTRA_EVENT_ID = "extra_event";
//    int eventId = 001;
//    // Build intent for notification content
//    Intent viewIntent = new Intent(this.getApplicationContext(), SensorService.class);
//    viewIntent.putExtra(EXTRA_EVENT_ID,eventId);
//    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.getApplicationContext())
//            .setSmallIcon(R.drawable.ic_launcher)
//            .setContentTitle(title)
//            .setContentText(message).extend(new NotificationCompat.WearableExtender()
//            .setCustomSizePreset(NotificationCompat.WearableExtender.SIZE_FULL_SCREEN));
//
//    // Get an instance of the NotificationManager service
//    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getApplicationContext());
//    // Build the notification and issues it with notification manager.
//    notificationManager.notify(notificationId,notificationBuilder.build());
//  }
//
//}
