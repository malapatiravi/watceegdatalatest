package projects.lab.research.eegwatchdata;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class MobileHome extends AppCompatActivity implements WatchDataCollector.StatusUpdater
{
  private final int REQUEST_WRITE_STORAGE       = 112;
  private final int REQUEST_RESOLVE_ERROR       = 1000;
  private static final String TAG = "Home";

  private Spinner                   activitySelector;
  private Spinner                   sessionSelector;
  private NeuroSkyDataCollector     nsCollector;
  private WatchDataCollector        watchCollector;
  private String Username                       = "Bob";
  private String dataFolder                     = Username;
  private Button            connectWatchBtn;
  private Button            connectNSBtn;
  private Button            logoutBtn;
  private Button            startDataCollectionBtn;
  private TextView          nsStatusTV;
  private TextView          watchStatusTV;
  private TextView          globalStatusTV;
  private boolean collectionRunning = false;

  private Context context;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_mobile_home);
    context = this;
    setupSpinners();
    Intent currentIntent = getIntent();
    GetAndSetExtras(currentIntent);
    CheckWritePermission();
    connectWatchBtn                 = (Button)   findViewById(R.id.smartwatch_connect_btn);
    connectNSBtn                    = (Button)   findViewById(R.id.neurosky_connect_btn);
    startDataCollectionBtn          = (Button)   findViewById(R.id.start_data_collection_btn);
    logoutBtn                       = (Button)   findViewById(R.id.logout_btn);
    nsStatusTV                      = (TextView) findViewById(R.id.neurosky_status_text);
    watchStatusTV                   = (TextView) findViewById(R.id.smartwatch_status_text);
    globalStatusTV                  = (TextView) findViewById(R.id.global_status_tv);

    nsCollector = new NeuroSkyDataCollector(this, nsStatusTV, connectNSBtn);

    //connectWatchBtn.setEnabled(false);
    connectWatchBtn       .setOnClickListener(smartWatchButtonClickListener);
    connectNSBtn          .setOnClickListener(nsButtonClickListener);
    startDataCollectionBtn.setOnClickListener(startDataCollectionListener);
    logoutBtn             .setOnClickListener(logoutClickListener);

    startDataCollectionBtn.setText(getResources().getString(R.string.start_data_collection));
    //new CheckWatchConnection(3).start();
  }
  /**************************************************/
  /*Methods & Implemented variables                 */
  /**************************************************/
  private void setupSpinners()
  {
    activitySelector = (Spinner) findViewById(R.id.spinner_activity_selector);
    sessionSelector = (Spinner) findViewById(R.id.spinner_session_selector);
    String[] ActivitySpinnerItems = {"One", "Two", "Three", "Four"};
    String[] SessionSpinnerItems = {"One", "Two", "Three", "Four"};
    ArrayAdapter<String> ActivityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, ActivitySpinnerItems);
    activitySelector.setAdapter(ActivityAdapter);
    ArrayAdapter<String> SessionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, SessionSpinnerItems);
    sessionSelector.setAdapter(SessionAdapter);
  }



  private void GetAndSetExtras(Intent CurrentIntent)
  {
    Username = (CurrentIntent.getStringExtra("Username")==null?Username:(CurrentIntent.getStringExtra("Username")));
    TextView UsernameTV = (TextView)findViewById(R.id.user_greetings_tv);
    UsernameTV.setText("Hello "+Username+"!");
    dataFolder = Username;
  }

  void CheckWritePermission()
  {
    boolean hasPermission = (ContextCompat.checkSelfPermission(MobileHome.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    if (!hasPermission)
    {
      ActivityCompat.requestPermissions(MobileHome.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }
  }

  /**************************************************/
  /* Button Click Listeners                         */
  /**************************************************/
  private View.OnClickListener smartWatchButtonClickListener = new View.OnClickListener()
  {
    @Override
    public void onClick(View view)
    {
      if (connectWatchBtn.getText() == (getResources().getString(R.string.disconnectText)))
      {
        watchCollector.destroy();
        watchCollector = null;
        connectWatchBtn.setText((getResources().getString(R.string.connectText)));
        tvSetMessage(watchStatusTV, getResources().getString(R.string.disconnectedText), SharedData.MSG_TYPE_ERROR);
      }
      else
      {
        watchCollector = new WatchDataCollector((MobileHome)context);
        tvSetMessage(watchStatusTV, getResources().getString(R.string.connectingText), SharedData.MSG_TYPE_DEFAULT);
        new CheckWatchConnection().start();
      }
      //connectWatchBtn.setText(getResources().getString(R.string.disconnectText));
    }
  };
  private View.OnClickListener nsButtonClickListener = new View.OnClickListener()
  {
    @Override
    public void onClick(View view)
    {
      Log.i(TAG, "Connecting to NeuroSky");
      nsCollector.toggleConnection();
    }
  };
  private View.OnClickListener startDataCollectionListener = new View.OnClickListener()
  {
    @Override
    public void onClick(View view)
    {
      if (startDataCollectionBtn.getText().toString().equals(getResources().getString(R.string.start_data_collection)))
      {
        String currentActivity = activitySelector.getSelectedItem().toString();
        String currentSession = sessionSelector.getSelectedItem().toString();
        watchCollector.setFile(dataFolder+"/"+currentSession, currentActivity);
        nsCollector.setFile(dataFolder+"/"+currentSession, currentActivity);
        Log.i(TAG, "Starting data collection now");
        //new CheckWatchConnection(-1).start();
        if (nsCollector.isNSConnected() && watchCollector.isWatchConnected())
        {
          collectionRunning = true;
          if (watchCollector.isWatchConnected()) watchCollector.startDataCollection();
          nsCollector.startDataCollection();
          connectNSBtn.setEnabled(false);
          connectWatchBtn.setEnabled(false);
          startDataCollectionBtn.setText(getResources().getString(R.string.stop_data_collection));
          tvSetMessage(globalStatusTV, getResources().getString(R.string.started_collection), SharedData.MSG_TYPE_DEFAULT);
        }
      }
      else
      {
        nsCollector.disconnectDevice();
        connectNSBtn.setEnabled(true);
        connectWatchBtn.setEnabled(true);
        startDataCollectionBtn.setText(getResources().getString(R.string.start_data_collection));
        watchCollector.stopDataCollection();
        collectionRunning = false;
      }
    }
  };
  private View.OnClickListener logoutClickListener = new View.OnClickListener()
  {
    @Override
    public void onClick(View view)
    {
      finish();
      Intent LoginScreenIntent = new Intent(context, LoginActivity.class);
      startActivity(LoginScreenIntent);
    }
  };

  @Override
  public void updateStatus(String message, int msgType)
  {
    tvSetMessage(globalStatusTV, message, msgType);
  }

  public class CheckWatchConnection extends Thread
  {
    public CheckWatchConnection()
    {
    }
    @Override
    public void run()
    {
      watchCollector.connectWatch();
      int i = 0;
      boolean isConnected = false;
      while (i < 20)
      {
        try                             {Thread.sleep(1000);}
        catch (InterruptedException e)  {e.printStackTrace();}
        i++;
        if (SharedData.isWearableConnected)
        {
          isConnected = true;
          break;
        }
        else
        {
          isConnected = false;
        }
      }
      final boolean finalIsConnected = isConnected;
      runOnUiThread(new Runnable()
      {
        @Override
        public void run()
        {
          if (finalIsConnected)
          {
            connectWatchBtn.setText(R.string.disconnectText);
            tvSetMessage(watchStatusTV, getResources().getString(R.string.connectedText), SharedData.MSG_TYPE_SUCCESS);
          }
          else
          {
            runOnUiThread(new Runnable()
            {
              @Override
              public void run()
              {
                connectWatchBtn.setText(R.string.connectText);
                tvSetMessage(watchStatusTV, getResources().getString(R.string.connectionTimeout), SharedData.MSG_TYPE_SUCCESS);
              }
            });
          }
        }
      });
    }
  }

  public void tvSetMessage(TextView tv, String message, int msgType)
  {
    Log.i(TAG, message);
    switch (msgType)
    {
      case (SharedData.MSG_TYPE_ERROR):
        tv.setText(message);
        tv.setTypeface(null, Typeface.NORMAL);
        tv.setTextColor(Color.RED);
        break;
      case (SharedData.MSG_TYPE_SUCCESS):
        tv.setTypeface(null, Typeface.NORMAL);
        tv.setText(message);
        tv.setTextColor(Color.GREEN);
        break;
      default:
        tv.setTypeface(null, Typeface.ITALIC);
        tv.setText(message);
        tv.setTextColor(Color.BLUE);
        break;
    }
  }


}
