package projects.lab.research.eegwatchdata;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by SuzzettPdl on 4/28/2016.
 */
public class DataSaver
{
  private final String ROOT_FOLDER = "WatchEEG_Data";
  private String FolderName;
  private String Filename;
  private String Root;
  private File Folder;
  private File ThisFile;
  private FileWriter FW;
  private PrintWriter PW;
  private String TAG = "DataSaver";
  public DataSaver(String FolderLoc, String FN)
  {
    Log.i(TAG, "Filename="+FN+", Folder="+FolderLoc);
    if (!isExternalStorageWritable()) Log.e(TAG, "External storage is not writable");
    Root = Environment.getExternalStorageDirectory().toString();
    File rootDir = Environment.getExternalStorageDirectory();
    if (!rootDir.canWrite())
    {
      Log.d(TAG, "cannot write in the root");
    }
    FolderName = FolderLoc;
    Filename = FN;
    Folder = new File(Root+"/"+ROOT_FOLDER+"/"+FolderName);
    if (!Folder.exists())
    {
      Log.i(TAG, "Dir does not exist, making one.");
      if (Folder.mkdirs()) Log.i(TAG, "Mkdir succeeded");
      else Log.i(TAG, "Mkdir failed");
    }
    String CompleteFilename = Folder+"/"+Filename;
    ThisFile = new File(Folder , Filename);
    //if (ThisFile.exists ()) ThisFile.delete ();
    try                   {FW = new FileWriter(CompleteFilename, true);}
    catch (IOException e) {e.printStackTrace();}
    PW = new PrintWriter(FW);
    this.SaveData("***********************Opened for writing***********************");
  }
  /* Checks if external storage is available for read and write */
  public boolean isExternalStorageWritable()
  {
    String state = Environment.getExternalStorageState();
    if (Environment.MEDIA_MOUNTED.equals(state))
    {

      return true;
    }
    return false;
  }

  public boolean SaveData(String SS)
  {
    PW.write(SS);
    PW.println();
    PW.flush();
    return true;
  }

  public void Close()
  {
    PW.close();
    try {FW.close();}
    catch (IOException e) {e.printStackTrace();}
  }

  public String GetPath()
  {
    return ThisFile.getAbsolutePath();
  }
}
