package projects.lab.research.eegwatchdata;

import android.bluetooth.BluetoothAdapter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.neurosky.thinkgear.TGDevice;
import com.neurosky.thinkgear.TGEegPower;

/**
 * Created by SuzzettPdl on 6/17/2016.
 */
public class NeuroSkyDataCollector
{
  private DataSaver dataFile;
  private TGDevice tgDevice;
  private MobileHome parentActivity;
  private final boolean NS_RAW_ENABLED            = true;
  private boolean saveData                        = false;
  private final String NS_FILENAME                = "NS.txt";
  private final String TAG                     = "NS Object";
  private TextView nsStateTV;
  private Button nsButton;
  private boolean nsIsConnected = false;

  public NeuroSkyDataCollector(MobileHome parentActivity, TextView nsStateTV, Button nsButton)
  {
    this.parentActivity = parentActivity;
    Log.i(TAG, "parent activity is null? "+(parentActivity==null));
    //parentActivity.get
    this.nsStateTV = (TextView)parentActivity.findViewById(R.id.neurosky_status_text);
    this.nsButton = (Button)parentActivity.findViewById(R.id.neurosky_connect_btn);
  }
  public void setFile(String dataFolder, String filenamePrefix)
  {
    this.dataFile = new DataSaver(dataFolder, filenamePrefix+"_"+NS_FILENAME);
  }
  public void connectDevice()
  {
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if(bluetoothAdapter == null)
    {
      // Alert user that Bluetooth is not available
      Toast.makeText(this.parentActivity, "Bluetooth not available", Toast.LENGTH_LONG).show();
      return;
    }
    /* create the TGDevice */
    tgDevice = new TGDevice(bluetoothAdapter, NeuroSkyDataHandler);
    nsEnable();
    handleNSStates(tgDevice.getState());
  }

  public void toggleConnection()
  {
    String currentStatus = nsButton.getText().toString();
    if (currentStatus.equals(parentActivity.getResources().getString(R.string.disconnectText)))
    {
      this.disconnectDevice();
      nsButton.setText(parentActivity.getResources().getString(R.string.connectText));
    }
    else if (currentStatus.equals(parentActivity.getResources().getString(R.string.connectText)))
    {
      this.connectDevice();
      nsButton.setText(parentActivity.getResources().getString(R.string.disconnectText));
    }
  }
  private void nsEnable()
  {
    if(tgDevice.getState() != TGDevice.STATE_CONNECTING && tgDevice.getState() != TGDevice.STATE_CONNECTED) tgDevice.connect(NS_RAW_ENABLED);
  }
  public void disconnectDevice()
  {
    tgDevice.stop();
    tgDevice.close();
  }

  public void saveData(String dataToSave)
  {
    if (saveData) dataFile.SaveData(dataToSave);
  }

  public void startDataCollection()
  {
    tgDevice.start();
    saveData = true;
  }

  public void pauseDataCollection()
  {
    saveData = false;
    tgDevice.stop();
  }

  private final Handler NeuroSkyDataHandler = new Handler()
  {
    @Override
    public void handleMessage(Message msg)
    {
      switch (msg.what)
      {
        case TGDevice.MSG_STATE_CHANGE:
          handleNSStates(msg.arg1);
          break;
        case TGDevice.MSG_POOR_SIGNAL:
          saveData(System.currentTimeMillis()+",PoorSignal," + msg.arg1);
          break;
        case TGDevice.MSG_RAW_DATA:
          saveData(System.currentTimeMillis()+",Raw," + msg.arg1);
          break;
        case TGDevice.MSG_HEART_RATE:
          saveData(System.currentTimeMillis()+",HeartRate," + msg.arg1);
          break;
        case TGDevice.MSG_ATTENTION:
          saveData(System.currentTimeMillis()+",Attention," + msg.arg1);
          break;
        case TGDevice.MSG_MEDITATION:
          saveData(System.currentTimeMillis()+",Meditation," + msg.arg1);
          break;
        case TGDevice.MSG_BLINK:
          saveData(System.currentTimeMillis()+",Blink," + msg.arg1);
          break;
        case TGDevice.MSG_RAW_COUNT:
          saveData(System.currentTimeMillis()+",RawCount," + msg.arg1);
          break;
        case TGDevice.MSG_LOW_BATTERY:
          Toast.makeText(parentActivity, "Low neurosky battery!", Toast.LENGTH_SHORT).show();
          break;
        case TGDevice.MSG_RAW_MULTI:
          saveData(System.currentTimeMillis()+",RawMulti,"+msg.obj);
          break;
        case TGDevice.MSG_EEG_POWER:
          TGEegPower ep = (TGEegPower) msg.obj;//(TGEegPower)msg.arg1;
          saveData(System.currentTimeMillis()+",Delta,"+ep.delta);
          saveData(System.currentTimeMillis()+",highAlpha,"+ep.highAlpha);
          saveData(System.currentTimeMillis()+",lowAlpha,"+ep.lowAlpha);
          saveData(System.currentTimeMillis()+",lowBeta,"+ep.lowBeta);
          saveData(System.currentTimeMillis()+",highBeta,"+ep.highBeta);
          saveData(System.currentTimeMillis()+",lowGamma,"+ep.lowGamma);
          saveData(System.currentTimeMillis()+",midGamma,"+ep.midGamma);
          saveData(System.currentTimeMillis()+",theta,"+ep.theta);
          break;
        default:
          break;
      }
    }
  };

  private void handleNSStates(final int State)
  {
    switch (State) {
      case TGDevice.STATE_IDLE:
        nsChangeState("Idle", SharedData.MSG_TYPE_DEFAULT);
        break;
      case TGDevice.STATE_CONNECTING:
        nsChangeState("Connecting", SharedData.MSG_TYPE_DEFAULT);
        break;
      case TGDevice.STATE_CONNECTED:
        nsChangeState("Connected", SharedData.MSG_TYPE_SUCCESS);
        break;
      case TGDevice.STATE_NOT_FOUND:
        nsChangeState("Can't find", SharedData.MSG_TYPE_ERROR);
        break;
      case TGDevice.STATE_NOT_PAIRED:
        nsChangeState("Not Paired", SharedData.MSG_TYPE_ERROR);
        break;
      case TGDevice.STATE_DISCONNECTED:
        nsChangeState("Disconnected", SharedData.MSG_TYPE_ERROR);
        break;
    }
  }

  private void nsChangeState(String message, int msgType)
  {
    Log.i(TAG, message);
    TextView nsStateTV = (TextView)parentActivity.findViewById(R.id.neurosky_status_text);
    switch(msgType)
    {
      case (SharedData.MSG_TYPE_ERROR):
        nsStateTV.setText(message);
        nsStateTV.setTypeface(null, Typeface.NORMAL);
        nsStateTV.setTextColor(Color.RED);
        nsIsConnected = false;
        nsButton.setText(parentActivity.getResources().getString(R.string.connectText));
        break;
      case (SharedData.MSG_TYPE_SUCCESS):
        nsStateTV.setTypeface(null, Typeface.NORMAL);
        nsStateTV.setText(message);
        nsStateTV.setTextColor(Color.GREEN);
        nsIsConnected = true;
        nsButton.setText(parentActivity.getResources().getString(R.string.disconnectText));
        break;
      default:
        nsStateTV.setTypeface(null, Typeface.ITALIC);
        nsStateTV.setText(message);
        nsStateTV.setTextColor(Color.BLUE);
        nsIsConnected = false;
        nsButton.setText(parentActivity.getResources().getString(R.string.connectText));
        break;
    }
  }

  public boolean isNSConnected()
  {
    return nsIsConnected;
  }
}
