package projects.lab.research.eegwatchdata;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.Manifest;
import android.os.Handler;

import com.choosemuse.libmuse.Accelerometer;
import com.choosemuse.libmuse.AnnotationData;
import com.choosemuse.libmuse.ConnectionState;
import com.choosemuse.libmuse.Eeg;
import com.choosemuse.libmuse.LibmuseVersion;
import com.choosemuse.libmuse.MessageType;
import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseConfiguration;
import com.choosemuse.libmuse.MuseConnectionListener;
import com.choosemuse.libmuse.MuseConnectionPacket;
import com.choosemuse.libmuse.MuseDataListener;
import com.choosemuse.libmuse.MuseDataPacket;
import com.choosemuse.libmuse.MuseFileFactory;
import com.choosemuse.libmuse.MuseFileReader;
import com.choosemuse.libmuse.MuseFileWriter;
import com.choosemuse.libmuse.MuseListener;
import com.choosemuse.libmuse.MuseManagerAndroid;
import com.choosemuse.libmuse.MuseVersion;
import com.choosemuse.libmuse.Result;
import com.choosemuse.libmuse.ResultLevel;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
/**
 * Created by SuzzettPdl on 7/11/2016.
 */
public class MuseDataCollector
{
  private MuseManagerAndroid manager;
  private Muse muse;
  private ConnectionListener connectionListener;
  private DataListener dataListener;

  private final double[] eegBuffer = new double[6];
  private boolean eegStale;
  private final double[] alphaBuffer = new double[6];
  private boolean alphaStale;
  private final double[] accelBuffer = new double[3];
  private boolean accelStale;

  private boolean dataTransmission = true;
  private final AtomicReference<MuseFileWriter> fileWriter = new AtomicReference<>();

  private final AtomicReference<Handler> fileHandler = new AtomicReference<>();

  private Context parentContext;
  private String TAG = "MuseDataCollector";
  public MuseDataCollector(Context parentContext)
  {
    this.parentContext = parentContext;
    manager = MuseManagerAndroid.getInstance();
    manager.setContext(this.parentContext);
    Log.i(TAG, "LibMuse version=" + LibmuseVersion.instance().getString());
    WeakReference<MobileHome> weakActivity = new WeakReference<MobileHome>((MobileHome)parentContext);
    // Register a listener to receive connection state changes.
    connectionListener = new ConnectionListener(weakActivity);
    // Register a listener to receive data from a Muse.
    dataListener = new DataListener(weakActivity);
    // Register a listener to receive notifications of what Muse headbands
    // we can connect to.
    manager.setMuseListener(new MuseL(weakActivity));
    // Muse 2016 (MU-02) headbands use Bluetooth Low Energy technology to
    // simplify the connection process.  This requires access to the COARSE_LOCATION
    // or FINE_LOCATION permissions.  Make sure we have these permissions before
    // proceeding.
    ensurePermissions();
    // Start up a thread for asynchronous file operations.
    // This is only needed if you want to do File I/O.
    fileThread.start();
  }

  public void pauseDevice()
  {
    manager.stopListening();
  }

  /**
   * The ACCESS_COARSE_LOCATION permission is required to use the
   * Bluetooth Low Energy library and must be requested at runtime for Android 6.0+
   * On an Android 6.0 device, the following code will display 2 dialogs,
   * one to provide context and the second to request the permission.
   * On an Android device running an earlier version, nothing is displayed
   * as the permission is granted from the manifest.
   *
   * If the permission is not granted, then Muse 2016 (MU-02) headbands will
   * not be discovered and a SecurityException will be thrown.
   */
  private void ensurePermissions() {

    if (ContextCompat.checkSelfPermission(parentContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      // We don't have the ACCESS_COARSE_LOCATION permission so create the dialogs asking
      // the user to grant us the permission.

      DialogInterface.OnClickListener buttonListener =
      new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface dialog, int which)
        {
          dialog.dismiss();
          ActivityCompat.requestPermissions((MobileHome)parentContext, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        }
      };

      // This is the context dialog which explains to the user the reason we are requesting
      // this permission.  When the user presses the positive (I Understand) button, the
      // standard Android permission dialog will be displayed (as defined in the button
      // listener above).
      AlertDialog introDialog = new AlertDialog.Builder(parentContext)
              .setTitle(R.string.permission_dialog_title)
              .setMessage(R.string.permission_dialog_description)
              .setPositiveButton(R.string.permission_dialog_understand, buttonListener)
              .create();
      introDialog.show();
    }
  }

  //--------------------------------------
  // Listeners

  /**
   * You will receive a callback to this method each time a headband is discovered.
   * In this example, we update the spinner with the MAC address of the headband.
   */
  public void museListChanged()
  {
    final List<Muse> list = manager.getMuses();

  }

  /**
   * You will receive a callback to this method each time there is a change to the
   * connection state of one of the headbands.
   * @param p     A packet containing the current and prior connection states
   * @param muse  The headband whose state changed.
   */
  public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse)
  {
    final ConnectionState current = p.getCurrentConnectionState();

    // Format a message to show the change of connection state in the UI.
    final String status = p.getPreviousConnectionState() + " -> " + current;
    Log.i(TAG, status);

    // Update the UI with the change in connection state.
    //...
    if (current == ConnectionState.DISCONNECTED) {
      Log.i(TAG, "Muse disconnected:" + muse.getName());
      // Save the data file once streaming has stopped.
      saveFile();
      // We have disconnected from the headband, so set our cached copy to null.
      this.muse = null;
    }
  }

  /**
   * You will receive a callback to this method each time the headband sends a MuseDataPacket
   * that you have registered.  You can use different listeners for different packet types or
   * a single listener for all packet types as we have done here.
   * @param p     The data packet containing the data from the headband (eg. EEG data)
   * @param muse  The headband that sent the information.
   */
  public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {writeDataPacketToFile(p);

    // valuesSize returns the number of data values contained in the packet.
    final long n = p.valuesSize();
    switch (p.packetType()) {
      case EEG:
        assert(eegBuffer.length >= n);
        getEegChannelValues(eegBuffer,p);
        eegStale = true;
        break;
      case ACCELEROMETER:
        assert(accelBuffer.length >= n);
        getAccelValues(p);
        accelStale = true;
        break;
      case ALPHA_RELATIVE:
        assert(alphaBuffer.length >= n);
        getEegChannelValues(alphaBuffer,p);
        alphaStale = true;
        break;
      case BATTERY:
      case DRL_REF:
      case QUANTIZATION:
      default:
        break;
    }
  }

  /**
   * You will receive a callback to this method each time an artifact packet is generated if you
   * have registered for the ARTIFACTS data type.  MuseArtifactPackets are generated when
   * eye blinks are detected, the jaw is clenched and when the headband is put on or removed.
   * @param p     The artifact packet with the data from the headband.
   * @param muse  The headband that sent the information.
   */
  public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
  }

  /**
   * Helper methods to get different packet values.  These methods simply store the
   * data in the buffers for later display in the UI.
   *
   * getEegChannelValue can be used for any EEG or EEG derived data packet type
   * such as EEG, ALPHA_ABSOLUTE, ALPHA_RELATIVE or HSI_PRECISION.  See the documentation
   * of MuseDataPacketType for all of the available values.
   * Specific packet types like ACCELEROMETER, GYRO, BATTERY and DRL_REF have their own
   * getValue methods.
   */
  private void getEegChannelValues(double[] buffer, MuseDataPacket p) {
    buffer[0] = p.getEegChannelValue(Eeg.EEG1);
    buffer[1] = p.getEegChannelValue(Eeg.EEG2);
    buffer[2] = p.getEegChannelValue(Eeg.EEG3);
    buffer[3] = p.getEegChannelValue(Eeg.EEG4);
    buffer[4] = p.getEegChannelValue(Eeg.AUX_LEFT);
    buffer[5] = p.getEegChannelValue(Eeg.AUX_RIGHT);
  }

  private void getAccelValues(MuseDataPacket p) {
    accelBuffer[0] = p.getAccelerometerValue(Accelerometer.X);
    accelBuffer[1] = p.getAccelerometerValue(Accelerometer.Y);
    accelBuffer[2] = p.getAccelerometerValue(Accelerometer.Z);
  }
  //--------------------------------------
  // File I/O

  /**
   * We don't want to block the UI thread while we write to a file, so the file
   * writing is moved to a separate thread.
   */
  private final Thread fileThread = new Thread() {
    @Override
    public void run() {
      Looper.prepare();
      fileHandler.set(new Handler());
      final File dir = parentContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
      final File file = new File(dir, "new_muse_file.muse" );
      // MuseFileWriter will append to an existing file.
      // In this case, we want to start fresh so the file
      // if it exists.
      if (file.exists()) {
        file.delete();
      }
      Log.i(TAG, "Writing data to: " + file.getAbsolutePath());
      fileWriter.set(MuseFileFactory.getMuseFileWriter(file));
      Looper.loop();
    }
  };

  /**
   * Writes the provided MuseDataPacket to the file.  MuseFileWriter knows
   * how to write all packet types generated from LibMuse.
   * @param p     The data packet to write.
   */
  private void writeDataPacketToFile(final MuseDataPacket p) {
    Handler h = fileHandler.get();
    if (h != null) {
      h.post(new Runnable() {
        @Override
        public void run() {
          fileWriter.get().addDataPacket(0, p);
        }
      });
    }
  }

  /**
   * Flushes all the data to the file and closes the file writer.
   */
  private void saveFile() {
    Handler h = fileHandler.get();
    if (h != null) {
      h.post(new Runnable() {
        @Override public void run() {
          MuseFileWriter w = fileWriter.get();
          // Annotation strings can be added to the file to
          // give context as to what is happening at that point in
          // time.  An annotation can be an arbitrary string or
          // may include additional AnnotationData.
          w.addAnnotationString(0, "Disconnected");
          w.flush();
          w.close();
        }
      });
    }
  }

  /**
   * Reads the provided .muse file and prints the data to the logcat.
   * @param name  The name of the file to read.  The file in this example
   *              is assumed to be in the Environment.DIRECTORY_DOWNLOADS
   *              directory.
   */
  private void playMuseFile(String name) {

    File dir = parentContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
    File file = new File(dir, name);

    final String tag = "Muse File Reader";

    if (!file.exists()) {
      Log.w(tag, "file doesn't exist");
      return;
    }

    MuseFileReader fileReader = MuseFileFactory.getMuseFileReader(file);

    // Loop through each message in the file.  gotoNextMessage will read the next message
    // and return the result of the read operation as a Result.
    Result res = fileReader.gotoNextMessage();
    while (res.getLevel() == ResultLevel.R_INFO && !res.getInfo().contains("EOF")) {

      MessageType type = fileReader.getMessageType();
      int id = fileReader.getMessageId();
      long timestamp = fileReader.getMessageTimestamp();

      Log.i(tag, "type: " + type.toString() +
              " id: " + Integer.toString(id) +
              " timestamp: " + String.valueOf(timestamp));

      switch(type) {
        // EEG messages contain raw EEG data or DRL/REF data.
        // EEG derived packets like ALPHA_RELATIVE and artifact packets
        // are stored as MUSE_ELEMENTS messages.
        case EEG:
        case BATTERY:
        case ACCELEROMETER:
        case QUANTIZATION:
        case GYRO:
        case MUSE_ELEMENTS:
          MuseDataPacket packet = fileReader.getDataPacket();
          Log.i(tag, "data packet: " + packet.packetType().toString());
          break;
        case VERSION:
          MuseVersion version = fileReader.getVersion();
          Log.i(tag, "version" + version.getFirmwareType());
          break;
        case CONFIGURATION:
          MuseConfiguration config = fileReader.getConfiguration();
          Log.i(tag, "config" + config.getBluetoothMac());
          break;
        case ANNOTATION:
          AnnotationData annotation = fileReader.getAnnotation();
          Log.i(tag, "annotation" + annotation.getData());
          break;
        default:
          break;
      }

      // Read the next message.
      res = fileReader.gotoNextMessage();
    }
  }

  //--------------------------------------
  // Listener translators
  //
  // Each of these classes extend from the appropriate listener and contain a weak reference
  // to the activity.  Each class simply forwards the messages it receives back to the Activity.
  class MuseL extends MuseListener
  {
    final WeakReference<MobileHome> activityRef;

    MuseL(final WeakReference<MobileHome> activityRef) {
      this.activityRef = activityRef;
    }

    @Override
    public void museListChanged() {
      museListChanged();
    }
  }

  class ConnectionListener extends MuseConnectionListener
  {
    final WeakReference<MobileHome> activityRef;

    ConnectionListener(final WeakReference<MobileHome> activityRef) {
      this.activityRef = activityRef;
    }

    @Override
    public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {
      receiveMuseConnectionPacket(p, muse);
    }
  }

  class DataListener extends MuseDataListener
  {
    final WeakReference<MobileHome> activityRef;

    DataListener(final WeakReference<MobileHome> activityRef) {
      this.activityRef = activityRef;
    }

    @Override
    public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
      receiveMuseDataPacket(p, muse);
    }

    @Override
    public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
      receiveMuseArtifactPacket(p, muse);
    }
  }

}
