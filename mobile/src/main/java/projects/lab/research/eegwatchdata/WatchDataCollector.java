package projects.lab.research.eegwatchdata;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * Created by SuzzettPdl on 6/18/2016.
 */
public class WatchDataCollector  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DataApi.DataListener, MessageApi.MessageListener
{
  private static final String TAG = "WatchDataCollector";
  private GoogleApiClient mGoogleClient;
  private MobileHome parentActivity;
  private Queue<DataMap> dataLoads;
  public boolean startCommanded = false;
  private final String accFilename = "Acc.txt";
  private final String gyrFilename = "Gyr.txt";
  private final String rovFilename = "RoV.txt";
  private String dataFolder;
  private String filenamePrefix;
  public static boolean savingData = false;
  private int totalDataPoints = 0;
  private StatusUpdater statusUpdaterListener;

  public WatchDataCollector(MobileHome parentActivity)
  {
    this.parentActivity = parentActivity;
    dataLoads = new LinkedList<>();
    totalDataPoints = 0;
    totalSavings = 0;
    accDataFromWatch = new ArrayList<>();
    gyrDataFromWatch = new ArrayList<>();
    rovDataFromWatch = new ArrayList<>();
    totalSavings = 0;
    try
    {
      statusUpdaterListener = (StatusUpdater) parentActivity;
    }
    catch (ClassCastException e)
    {
      throw new ClassCastException("Parent have not implemented StatusUpdater");
    }
    setupGoogleAPI();

  }

  public void setFile(String dataFolder, String filenamePrefix)
  {
    this.dataFolder = dataFolder;
    this.filenamePrefix = filenamePrefix;
  }

  public void destroy()
  {
    DataMap dataMap = new DataMap();
    dataMap.putLong("time", System.currentTimeMillis());
    dataMap.putString("command", SharedData.DISCONNECT_WATCH_CMD);
    new SendToDataLayerThread(SharedData.FROM_PHONE_DATA_PATH, dataMap).start();
  }
  public boolean isWatchConnected()
  {
    return SharedData.isWearableConnected;
  }
  public void connectWatch()
  {
    new CheckWearableConnected().execute();
  }

  public void startDataCollection()
  {
    DataMap dataMap = new DataMap();
    dataMap.putLong(SharedData.TIME_TAG, System.currentTimeMillis());
    dataMap.putString(SharedData.COMMAND_TAG, SharedData.START_DC);
    dataMap.putString(SharedData.DATA_FILE_PREFIX_TAG, filenamePrefix);
    dataMap.putString(SharedData.DATA_FOLDER_TAG, dataFolder);
    new SendToDataLayerThread(SharedData.FROM_PHONE_DATA_PATH, dataMap).start();
    startCommanded = true;
    //notifyWatch("EEGWatch", "started data collection");
    //new SaveData().start();
  }

  interface StatusUpdater
  {
    void updateStatus(String message, int messageType);
  }
  public int getTotalPoints()
  {
    return totalDataPoints;
  }

  boolean dataTransferInProgress = false;
  public void stopDataCollection()
  {
    dataTransferInProgress = true;
    statusUpdaterListener.updateStatus("Waiting for data...", SharedData.MSG_TYPE_DEFAULT);
    DataMap dataMap = new DataMap();
    dataMap.putLong(SharedData.TIME_TAG, System.currentTimeMillis());
    dataMap.putString(SharedData.COMMAND_TAG, SharedData.STOP_DC);
    new SendToDataLayerThread(SharedData.FROM_PHONE_DATA_PATH, dataMap).start();
    //notifyWatch("EEGWatch", "stopped data collection");
    startCommanded = false;
  }

  private void setupGoogleAPI()
  {
    mGoogleClient = new GoogleApiClient.Builder(this.parentActivity).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Wearable.API).build();
    mGoogleClient.connect();
  }

  private boolean validateConnection()
  {
    if (mGoogleClient.isConnected())
    {
      Log.i(TAG, "google API is connected");
      return true;
    }
    Log.i(TAG, "google API is not connected");
    ConnectionResult result = mGoogleClient.blockingConnect(SharedData.CONNECTION_TIMEOUT_MS, TimeUnit.MILLISECONDS);
    return result.isSuccess();
  }

  @Override
  public void onConnected(Bundle bundle)
  {
    Log.i(TAG, "Google API connected");
    Wearable.DataApi.addListener(mGoogleClient, this);
    Wearable.MessageApi.addListener( mGoogleClient, this );
  }

  @Override
  public void onConnectionSuspended(int i)
  {
    Log.i(TAG, "Google API connection suspended");
  }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult)
  {
    Log.i(TAG, "Google API: " + connectionResult.getErrorMessage());
  }

  private int totalSavings = 0;
  private int totalAccDataPackets = -1;
  private int totalGyrDataPackets = -1;
  private int totalRovDataPackets = -1;
  private ArrayList<ArrayList<String>> accDataFromWatch;
  private ArrayList<ArrayList<String>> gyrDataFromWatch;
  private ArrayList<ArrayList<String>> rovDataFromWatch;
  @Override
  public void onMessageReceived(MessageEvent messageEvent)
  {
    byte[] allDataBytes = messageEvent.getData();

    String msgPath = messageEvent.getPath();

    ArrayList<String> allData = getListFromByteArray(allDataBytes);
    if (msgPath.equals(SharedData.FROM_WEAR_DATA_PATH_ACC))
    {
      Log.i(TAG, "received "+allDataBytes.length+" bytes. MsgType is: "+msgPath);
      updateWithNewData(allData, SharedData.SENSOR_ACC_TAG);
      if (totalAccDataPackets >= accDataFromWatch.size())
      {
        totalSavings++;
        Log.d(TAG, "Saving acc data now. Total packets for acc: "+totalAccDataPackets);
        statusUpdaterListener.updateStatus("Saving acc data now...", SharedData.MSG_TYPE_DEFAULT);
        ArrayList<String> serializedData = getDataToSave(accDataFromWatch);
        saveData(serializedData, filenamePrefix + "_" + accFilename);
      }
    }
    if (msgPath.equals(SharedData.FROM_WEAR_DATA_PATH_GYR))
    {
      Log.i(TAG, "received "+allDataBytes.length+" bytes. MsgType is: "+msgPath);
      updateWithNewData(allData, SharedData.SENSOR_GYR_TAG);
      if (totalGyrDataPackets >= gyrDataFromWatch.size())
      {
        totalSavings++;
        Log.d(TAG, "Saving gyr data now. Total packets for gyr: "+totalGyrDataPackets);
        statusUpdaterListener.updateStatus("Saving gyr data now...", SharedData.MSG_TYPE_DEFAULT);
        ArrayList<String> serializedData = getDataToSave(gyrDataFromWatch);
        saveData(serializedData, filenamePrefix + "_" + gyrFilename);
      }
    }
    if (msgPath.equals(SharedData.FROM_WEAR_DATA_PATH_ROV))
    {
      Log.i(TAG, "received "+allDataBytes.length+" bytes. MsgType is: "+msgPath);
      updateWithNewData(allData, SharedData.SENSOR_ROV_TAG);
      if (totalRovDataPackets >= rovDataFromWatch.size())
      {
        totalSavings++;
        Log.d(TAG, "Saving rov data now. Total packets for rov: "+totalRovDataPackets);
        statusUpdaterListener.updateStatus("Saving rov data now...", SharedData.MSG_TYPE_DEFAULT);
        ArrayList<String> serializedData = getDataToSave(rovDataFromWatch);
        saveData(serializedData, filenamePrefix + "_" + rovFilename);
      }
    }
    if (msgPath.equals(SharedData.FROM_WEAR_DATA_PATH_CMD))
    {
      String cmdMessage = allData.get(0);
      if (cmdMessage.equals("Finished")) totalSavings++;
    }
    if (totalSavings>= 3)
    {
      statusUpdaterListener.updateStatus("Data saving is complete", SharedData.MSG_TYPE_SUCCESS);
    }
  }

  private void updateWithNewData(ArrayList<String> currData, String msgType)
  {
    String packetInfo = currData.get(0);
    String[] packetInfoParts = packetInfo.split("-");
    Log.i(TAG, "data: "+Arrays.toString(currData.toArray()));
    if (packetInfoParts[0].equals("Pack"))
    {
      if (msgType.equals(SharedData.SENSOR_ACC_TAG)) accDataFromWatch.add(currData);
      if (msgType.equals(SharedData.SENSOR_GYR_TAG)) gyrDataFromWatch.add(currData);
      if (msgType.equals(SharedData.SENSOR_ROV_TAG)) rovDataFromWatch.add(currData);
    }
    else if (packetInfoParts[0].equals("Last"))
    {
      if (msgType.equals(SharedData.SENSOR_ACC_TAG)) totalAccDataPackets = Integer.parseInt(packetInfoParts[1]);
      if (msgType.equals(SharedData.SENSOR_GYR_TAG)) totalGyrDataPackets = Integer.parseInt(packetInfoParts[1]);
      if (msgType.equals(SharedData.SENSOR_ROV_TAG)) totalRovDataPackets = Integer.parseInt(packetInfoParts[1]);
    }
  }

  private ArrayList<String> getDataToSave(ArrayList<ArrayList<String>> origData)
  {
    ArrayList<ArrayList<String>> fullDataToSave = new ArrayList<>(origData.size());
    for (int i = 0; i < origData.size(); i++)
    {
      String packInfo = origData.get(i).get(0);
      Log.i(TAG, "packInfo0: "+packInfo);
      int currPacketNum = Integer.parseInt(packInfo.split("-")[1]);
      origData.get(i).remove(0);
      fullDataToSave.add(currPacketNum, origData.get(i));
    }
    ArrayList<String> serializedData = new ArrayList<>();
    for (int i = 0; i < fullDataToSave.size(); i++)
    {
      for (int j = 0; j < fullDataToSave.get(i).size(); j++)
      {
        serializedData.add(fullDataToSave.get(i).get(j));
      }
    }
    return serializedData;
  }
  public ArrayList<String> getListFromByteArray(byte[] data)
  {
    // read from byte array
    ByteArrayInputStream bais = new ByteArrayInputStream(data);
    ArrayList<String> returnData = new ArrayList<>();
    DataInputStream in = new DataInputStream(bais);
    try
    {
      while (in.available() > 0)
      {
        String element = null;
        element = in.readUTF();
        returnData.add(element);
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return returnData;
  }
  @Override
  public void onDataChanged(DataEventBuffer dataEventBuffer)
  {
    Log.i(TAG, "data changed");
    for (DataEvent event : dataEventBuffer)
    {
      // Check the data type
      if (event.getType() == DataEvent.TYPE_CHANGED)
      {
        // Check the data path
        String path = event.getDataItem().getUri().getPath();
        if (path.equals(SharedData.FROM_WEAR_DATA_PATH))
        {
          DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
          DataMap dataMap = dataMapItem.getDataMap();
          if (dataMap.containsKey(SharedData.SENSOR_ACC_TAG))
          {
            Log.i(TAG, "Received ACC data");
            Asset accDataAsset = dataMapItem.getDataMap().getAsset(SharedData.SENSOR_ACC_TAG);
            byte[] accDataByte = accDataAsset.getData();
            String[] allAccData = (new String(accDataByte)).split("::");
            saveData(allAccData, filenamePrefix + "_" + accFilename);
          }
          if (dataMap.containsKey(SharedData.SENSOR_GYR_TAG))
          {
            Log.i(TAG, "Received GYR data");
            Asset gyrDataAsset = dataMapItem.getDataMap().getAsset(SharedData.SENSOR_GYR_TAG);
            byte[] gyrDataByte = gyrDataAsset.getData();
            String[] allGyrData = (new String(gyrDataByte)).split("::");
            saveData(allGyrData, filenamePrefix + "_" + gyrFilename);
          }
          if (dataMap.containsKey(SharedData.SENSOR_ROV_TAG))
          {
            Asset rovDataAsset = dataMapItem.getDataMap().getAsset(SharedData.SENSOR_ROV_TAG);
            byte[] rovDataByte = rovDataAsset.getData();
            Log.i(TAG, "Received ROV data: "+rovDataByte.length);
            String[] allRovData = (new String(rovDataByte)).split("::");
            saveData(allRovData, filenamePrefix + "_" + rovFilename);
          }
        }
      }
    }
  }

  private void saveData(String[] data, String fileName)
  {
    Log.d(TAG, "Saving to "+fileName+": "+data+"");
    DataSaver dataSaver = new DataSaver(dataFolder, fileName);
    for (int i = 1; i < data.length; i++)
    {
      dataSaver.SaveData(data[i]);
    }
    dataSaver.Close();
  }

  private void saveData(List<String> data, String fileName)
  {
    Log.d(TAG, "Saving to "+fileName+": "+data+"");
    DataSaver dataSaver = new DataSaver(dataFolder, fileName);
    for (int i = 0; i < data.size(); i++)
    {
      dataSaver.SaveData(data.get(i));
    }
    dataSaver.Close();
  }

  class SendToDataLayerThread extends Thread
  {
    String path;
    DataMap dataMap;

    // Constructor for sending data objects to the data layer
    SendToDataLayerThread(String path, DataMap data)
    {
      this.path = path;
      this.dataMap = data;
    }

    public void run()
    {
      // Construct a DataRequest and send over the data layer
      PutDataMapRequest putDMR = PutDataMapRequest.create(path);
      putDMR.getDataMap().putAll(dataMap);
      PutDataRequest request = putDMR.asPutDataRequest();
      DataApi.DataItemResult result = Wearable.DataApi.putDataItem(mGoogleClient, request).await();
      if (result.getStatus().isSuccess())
      {
        Log.v(TAG, "DataMap: " + dataMap + " sent successfully to data layer ");
      }
      else
      {
        if (dataMap.get(SharedData.COMMAND_TAG).equals(SharedData.START_DC)) startCommanded = false;
        // Log an error
        Log.v("myTag", "ERROR: failed to send DataMap to data layer");
      }
    }
  }

  private class CheckWearableConnected extends AsyncTask<Void, Void, Void>
  {
    @Override
    protected Void doInBackground(Void... voids)
    {
      NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
      if (nodes != null && nodes.getNodes().size() > 0)
      {
        DataMap dataMap = new DataMap();
        dataMap.putLong("time", System.currentTimeMillis());
        dataMap.putString(SharedData.COMMAND_TAG, SharedData.CONNECT_WATCH_CMD);
        PutDataMapRequest putDMR = PutDataMapRequest.create(SharedData.FROM_PHONE_DATA_PATH);
        putDMR.getDataMap().putAll(dataMap);
        PutDataRequest request = putDMR.asPutDataRequest();
        DataApi.DataItemResult result = Wearable.DataApi.putDataItem(mGoogleClient, request).await();
        if (result.getStatus().isSuccess())
        {
          SharedData.isWearableConnected = true;
          Log.v(TAG, "DataMap: " + dataMap + " sent successfully to data layer ");
        }
        else
        {
          // Log an error
          SharedData.isWearableConnected = false;
          Log.v("myTag", "ERROR: failed to send DataMap to data layer");
        }
      }
      else
      {
        SharedData.isWearableConnected = false;
      }
      return null;
    }
  }

}
