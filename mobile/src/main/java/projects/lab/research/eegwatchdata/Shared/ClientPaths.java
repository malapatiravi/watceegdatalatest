package projects.lab.research.eegwatchdata.Shared;

public class ClientPaths {
    public static final String START_MEASUREMENT = "/start";
    public static final String STOP_MEASUREMENT = "/stop";
}
