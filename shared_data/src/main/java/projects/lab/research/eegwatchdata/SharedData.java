package projects.lab.research.eegwatchdata;

import com.google.android.gms.wearable.DataMap;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by SuzzettPdl on 6/19/2016.
 */
public class SharedData
{
  public static final int MSG_TYPE_ERROR                = 0;
  public static final int MSG_TYPE_SUCCESS              = 1;
  public static final int MSG_TYPE_DEFAULT              = 2;
  public static final String MSG_TYPE_ERROR_TEXT        = "ERROR";
  public static final String MSG_TYPE_SUCCESS_TEXT      = "SUCCESS";
  public static final String MSG_TYPE_DEFAULT_TEXT      = "DEFAULT";
  public static final String FROM_WEAR_DATA_PATH_ACC = "/sensor_data_acc";
  public static final String FROM_WEAR_DATA_PATH_GYR = "/sensor_data_gyr";
  public static final String FROM_WEAR_DATA_PATH_ROV = "/sensor_data_rov";
  public static final String FROM_WEAR_DATA_PATH_CMD = "/sensor_data_cmd";
  public static final String FROM_WEAR_DATA_PATH = "/sensor_data";
  public static final String FROM_WEAR_DATA_CAPABILITY = "sensor_data";
  public static final String FROM_PHONE_DATA_PATH = "/mobile_data";
  public static final String COMMAND_TAG = "command";
  public static final String TIME_TAG = "time";
  public static final String MESSAGE_TAG = "message";
  public static final String CONNECT_WATCH_CMD = "Connect Watch";
  public static final String DISCONNECT_WATCH_CMD = "Disconnect Watch";
  public static final String FINISH_WEAR_ACTIVITY = "Finish Activity";
  public static final String START_DC = "Start data collection";
  public static final String STOP_DC = "Stop data collection";
  public static final long CONNECTION_TIMEOUT_MS = 15000;
  public static boolean isWearableConnected = false;
  public static final int SENSOR_ACC = 0;
  public static final int SENSOR_GYR = 1;
  public static final int SENSOR_ROV = 2;
  public static final String SENSOR_ACC_TAG = "ACC_DATA";
  public static final String SENSOR_GYR_TAG = "GYR_DATA";
  public static final String SENSOR_ROV_TAG = "ROV_DATA";
  public static final String NODE_ID_TAG = "NODE_ID";
  public static final String DATA_LOAD_DELIMETER = "::";
  public static final String DATA_FOLDER_TAG = "dataFolder";
  public static final String DATA_FILE_PREFIX_TAG = "fileNamePrefix";
  public static final int MAX_TRANSFER_BYTES = 512000;

  public static final int SENDING_BUFFER_SIZE = 3;
  public static ConcurrentLinkedQueue<String> wearToPhoneBuffer_acc = new ConcurrentLinkedQueue<>();
  public static ConcurrentLinkedQueue<String> wearToPhoneBuffer_gyr = new ConcurrentLinkedQueue<>();
  public static ConcurrentLinkedQueue<String> wearToPhoneBuffer_rov = new ConcurrentLinkedQueue<>();

  public static ConcurrentLinkedQueue<DataMap> accBufferFromWear = new ConcurrentLinkedQueue<>();
  public static Queue<DataMap> wearSensorDataBuffer = new LinkedList<>();

  public boolean wearActivityIsOn = false;
}
